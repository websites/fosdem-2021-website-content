---
description: |
  <p>KDE is a worldwide 🌍 technology community, creators of the Plasma desktop
  and an ever-growing catalogue of free and open source applications that let you
  control your digital life.
  </p>
  <p>
    <a href="https://kde.org">
      <img src="/stands/kde_community/group.jpg" class="img-fluid">
    </a>
  </p>

layout: stand
logo: stands/kde_community/logo.png

showcase: |
 <p>Interact with KDE Community members, check out the latest advances in our software,
  including cool new features in Plasma, real world uses of KDE software for artists,
  educators, and end users in general, watch cool devices in action running Plasma Mobile
  and much more.</p>
  <p>Click below to go to the chat and live video demos:</p>
  <a href="https://chat.fosdem.org/#/room/#kde-stand:fosdem.org">
    <div style="  margin: 10px 200px 10px 200px; padding: 20px; background-color: red; text-align: center;">
      <h2 style="color: white">Chat and Demos</h2>
    </div>
  </a>
  <h2>Live Demos Schedule:</h2>
  <div>
    <table class="table table-striped table-bordered table-condensed">
      <tbody>
        <tr>
          <td colspan="5">Saturday</td>
        </tr>
      <thead>
        <tr>
         <td colspan="2">Title</td>
         <td>Demo Master</td>
         <td>Start</td>
         <td>End</td>
        </tr>
      </thead>
        <tr>
         <td class="c3">&nbsp;</td>
         <td><a href="https://www.plasma-mobile.org/">Plasma Mobile in 2022</a></td>
         <td>Bhushan Shah</td>
         <td>10:00</td>
         <td>11:00</td>
        </tr>
        <tr>
         <td class="c4">&nbsp;</td>
         <td><a href="https://invent.kde.org/pim/kalendar">Kalendar</a> and the case for Akonadi</td>
         <td>Claudio Cambra</td>
         <td>11:15</td>
         <td>12:15</td>
        </tr>
        <tr>
         <td class="c5">&nbsp;</td>
         <td><a href="https://eco.kde.org/">KDE Eco</a>: Free & Open Source Software:<br />User autonomy and transparency is good<br />for the environment too!</td>
         <td>Joseph De Veaugh-Geiss</td>
         <td>13:00</td>
         <td>14:00</td>
        </tr>
        <tr>
          <td colspan="5">Sunday</td>
        </tr>
      <thead>
        <tr>
         <td colspan="2">Title</td>
         <td>Demo Master</td>
         <td>Start</td>
         <td>End</td>
        </tr>
      </thead>
        <tr>
         <td class="c8">&nbsp;</td>
         <td><a href="https://pointieststick.com/">KDE Weekly News (Live!)</a></td>
         <td>Adam Szopa</td>
         <td>10:00</td>
         <td>10:15</td>
        </tr>
        <tr>
         <td class="c9">&nbsp;</td>
         <td>Plasma 5.24 tour</td>
         <td>KDE Promo team</td>
         <td>10:30</td>
         <td>11:00</td>
        </tr>
        <tr>
         <td class="c10">&nbsp;</td>
         <td>Live coding session with <a href="https://invent.kde.org/network/tokodon">Tokodon</a></td>
         <td>Carl Schwan</td>
         <td>12:00</td>
         <td>13:00</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div>
      <h2>Stickers and Merch</h2>
      <p><img src="/stands/kde_community/stickers_1500.png" class="img-fluid"></p>
      <p>We got Konqi Stickers for Matrix! Head over to <a href="https://chat.fosdem.org/#/room/#kde-stand:fosdem.org">KDE's chat room</a> and get your stickers now!</p>
      <p>If you would like to get T-shirts, polo shirts, hoodies, caps or any other KDE branded apparel, check out <a href="https://www.freewear.org/KDE">Freewear's online KDE shop</a>.</p>
  </div>
 
 <div style="display: flex">
        <div style= "flex: 40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%; " src="/stands/kde_community/kickoff.png">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h2>KDE's Plasma Desktop</h2>
            <p>The KDE Community builds the Plasma desktop. Plasma is customisable, flexible, full of features and lightweight.</p>
            <p>Plasma is available <a href="https://kde.org/distributions/">on most distros</a> and comes <a href="https://kde.org/hardware/">preinstalled on many laptops and ultrabooks</a>.</p>
      </div>
  </div>

  <div style="display: flex">
        <div style= "flex:40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%;" src="/stands/kde_community/phones_whitebg.png">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h2>Plasma Mobile</h2>
            <p>We also create Plasma Mobile, a free, open and privacy-protecting environment for phones, tablets and other mobile devices. You can learn more about Plasma Mobile at the <a href="https://www.plasma-mobile.org/">project's home page.</a>.</p>
        </div>
  </div>

  <div style="display: flex">
        <div style= "flex:40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%;" src="/stands/kde_community/apps.png">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h2>Applications</h2>
            <p>KDE produces and maintains a large number of high quality applications that cover from IDEs for developers and diagnostic tools for system monitoring, to programs for artists and educational software for teachers and students.</p>
            <p>You can find a comprehensive list of <a href="https://apps.kde.org/">KDE apps in our catalog</a>, but here are a few you may find interesting:</p>
        </div>    
  </div>

  <div style="display: flex">
        <div style= "flex:40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%;" src="/stands/kde_community/kdevelop.png">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h3><a href="https://www.kdevelop.org/">KDevelop</a></h3>
            <p>Is a cross-platform IDE for C, C++, Python, QML/JavaScript and PHP that significantly simplifies the creation of sophisticated applications. KDevelop supports Linux, FreeBSD, macOS and other Unix flavors as well as Microsoft Windows.</p>
        </div>
  </div>
  
  <div style="display: flex">
        <div style= "flex:40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%;" src="/stands/kde_community/krita.png">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h3><a href="https://krita.org/">Krita</a></h3>
            <p>Krita is an amazingly popular graphic design application for painters. It includes 100s of professionally developed brushes and features and is especially designed to help artists realize their vision.</p>
            <p>Krita is available for Linux, Windows and macOS, and their is version in development for Android that is touch-enabled.</p>
        </div>
  </div>
  
  <div style="display: flex">
        <div style= "flex:40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%;" src="/stands/kde_community/konsole.png">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h3><a href="https://konsole.kde.org/">Konsole</a></h3>
            <p>Konsole is arguably the most sophisticated terminal emulator in the known universe. Its features include the possibility of opening multiple terminals in tabs in the same window, splitting said tabs into panes, synchronizing commands across said tabs and panes, file previews (including graphics), opening links, text reflow (new!) and much more.</p>
        </div>
  </div>

  <div style="display: flex">
        <div style= "flex:40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%;" src="/stands/kde_community/kdenlive.jpg">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h3><a href="https://kdenlive.org">Kdenlive</a></h3>
            <p>Is a full-featured video editor that lets you apply effects, link clips with sophisticated transitions, correct color, dub audio and apply subtitles to you movies.</p>
        </div>
  </div>
  <div style="display: flex">
        <div style= "flex:40%; margin: 0px 50px 10px 50px;">
            <img style="width: 100%;" src="/stands/kde_community/gcompris.png">
        </div>
        <div style= "flex:60%; margin: 0px 10px 0px 10px;">
            <h3><a href="https://www.gcompris.net">GCompris</a></h3>
            <p>GCompris is a high quality educational software suite, including over 100 activities for children aged 2 to 10. Some of the activities are game orientated, but all are educational.</p>
        </div>
  </div>

themes:
- Desktop environments
title: KDE Community
website: https://kde.org
show_on_overview: true
chatroom: kde
---

<!--
new_this_year: |
  <link type="text/css" rel="stylesheet" href="/stands/kde_community/style.css">
  <p>KDE's Plasma desktop has had many features added; improved usability,
  stability and performance; and made a massive headway in support on Wayland </p>
  <p>Plasma Mobile has made a headway in stability and functionality and is now well-supported
    on dedicated hardware, such as the PinePhone and Librem 5. New apps specifically
    adapted to mobile hardware provide users with essential functionality they expect
    from a modern mobile OS. </p>
  <p>Kdenlive, Krita, LabPlot, Kdevelop, GCompris and
    many more classic KDE applications have improved stability and added features
    to the point they are competing successfully with traditionally dominant proprietary
    software in their respective niches </p>
  <p>We have added new apps, both for the
    desktop and mobile, to KDE's catalogue </p>
  <p>We have improved most of KDE frameworks,
    providing developers with tools that help them develop easily more visually appealing
    multiplatform and convergent software.</p>-->
